﻿using System;
using System.Collections.Generic;
using System.Text;

using BenchmarkDotNet.Attributes;

namespace performance
{
    [ShortRunJob]
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    public class LatencyBenchmarks
    {

        // take note that the number of elements must be reasonable huge
        // to see the impacts of these measurements properly. The array is
        // ( 1 << 24) * sizeof(int) big, which is: 67,108864 mb
        const int numberOfElements = 1 << 24;
        int[] ai = new int[numberOfElements];
        float[] af = new float[numberOfElements];

        public LatencyBenchmarks()
        {
            for (int j = 0; j < numberOfElements; j++)
                ai[j] = j;

            for (int j = 0; j < numberOfElements; j++)
                af[j] = j;
        }

        [Benchmark(Description = "Only a square root")]
        [Arguments(numberOfElements)]
        public void LatencySquareRoot(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0] = MathF.Sqrt(af[0]);
            }
        }

        [Benchmark(Description = "Square root and float operations base")]
        [Arguments(numberOfElements)]
        public void LatencySquareRootAndOthersBaseA(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0] = MathF.Sqrt(af[0]);
            }

            for(int j = 0; j < iterations; j++)
            {
                af[1] *= af[2];
                af[3] *= af[4];
                af[5] *= af[6];
                af[7] *= af[8];
                af[9] *= af[10];
                af[11] *= af[12];
                af[13] *= af[14];
            }
        }

        [Benchmark(Description = "Square root and float operations")]
        [Arguments(numberOfElements)]
        public void LatencySquareRootAndOthersA(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0] = MathF.Sqrt(af[0]);
                af[1] *= af[2];
                af[3] *= af[4];
                af[5] *= af[6];
                af[7] *= af[8];
                af[9] *= af[10];
                af[11] *= af[12];
                af[13] *= af[14];
            }
        }

        [Benchmark(Description = "Square root and integer operations")]
        [Arguments(numberOfElements)]
        public void LatencySquareRootAndOthersB(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0] = MathF.Sqrt(af[0]);
                ai[1] += ai[2];
                ai[3] += ai[4];
                ai[5] += ai[6];
                ai[7] += ai[8];
                ai[9] += ai[10];
                ai[11] += ai[12];
                ai[13] += ai[14];
            }
        }

        [Benchmark(Description = "Two square roots at once")]
        [Arguments(numberOfElements)]
        public void LatencyMultipleSquareRootsA(int iterations)
        {
            for (int j = 0; j < iterations; j += 2)
            {
                af[0] = MathF.Sqrt(af[0]);
                af[1] = MathF.Sqrt(af[1]);
            }
        }

        [Benchmark(Description = "Four square roots at once")]
        [Arguments(numberOfElements)]
        public void LatencyMultipleSquareRootsB(int iterations)
        {
            for (int j = 0; j < iterations; j += 4)
            {
                af[0] = MathF.Sqrt(af[0]);
                af[1] = MathF.Sqrt(af[1]);
                af[2] = MathF.Sqrt(af[2]);
                af[3] = MathF.Sqrt(af[3]);
            }
        }

        [Benchmark(Description = "Two square roots at once base")]
        [Arguments(numberOfElements)]
        public void LatencyMultipleSquareRootsBaseA(int iterations)
        {
            for (int j = 0; j < iterations; j += 2)
            {
                af[0] = MathF.Sqrt(af[0]);
            }
        }

        [Benchmark(Description = "Four square roots at once base")]
        [Arguments(numberOfElements)]
        public void LatencyMultipleSquareRootsBaseB(int iterations)
        {
            for (int j = 0; j < iterations; j += 4)
            {
                af[0] = MathF.Sqrt(af[0]);
            }
        }

        private struct Circle
        {

            /// <summary>
            /// The origin of the circle
            /// </summary>
            public float px, py;

            /// <summary>
            /// The radius of the circle
            /// </summary>
            public float radius { get; set; }

            public Circle(float px, float py, float radius)
            {
                this.px = px;
                this.py = py;
                this.radius = radius;
            }

        }

        private struct Ray
        {
            /// <summary>
            /// The origin of the ray
            /// </summary>
            public float ox, oy;

            /// <summary>
            /// The direction of the ray
            /// </summary>
            public float dx, dy;

            public Ray(float ox, float oy, float dx, float dy)
            {
                this.ox = ox;
                this.oy = oy;
                this.dx = dx;
                this.dy = dy;
            }
        }

        [Benchmark(Description = "Ray to circle base A")]
        [Arguments(numberOfElements)]
        public void LatencyCircleBaseA(int iterations)
        {
            // input
            Circle cir = new Circle(10, 10, 5);
            Ray ray = new Ray(10, 0, 0, 1);

            // 'output'
            bool intersect = false;
            float t = 0;

            for (int j = 0; j < iterations; j++)
            {
                // Step 0: Work everything out on paper!

                // Step 2: compute the substitutions.
                float p = ray.ox - cir.px;
                float q = ray.oy - cir.py;

                float r = 2 * p * ray.dx;
                float s = 2 * q * ray.dy;

                // Step 3: compute the substitutions, check if there is a collision.
                float a = ray.dx * ray.dx + ray.dy * ray.dy;
                float b = r + s;
                float c = p * p + q * q - cir.radius * cir.radius;

                float DSqrt = b * b - 4 * a * c;

                // no collision possible!
                if (DSqrt < 0)
                { intersect = false; }

                // Step 4: compute the substitutions.
                float D = (float)Math.Sqrt(DSqrt);

                //float t0 = (-b + D) / (2 * a);
                float t1 = (-b - D) / (2 * a);

                //float ti = Math.Min(t0, t1);
                if (t1 > 0 && t1 < t)
                {
                    t = t1;
                    intersect = true;
                }

                intersect = false;
            }
        }

        [Benchmark(Description = "Ray to circle base  B")]
        [Arguments(numberOfElements)]
        public void LatencyCircleBaseB(int iterations)
        {
            // input
            Circle cir = new Circle(10, 10, 5);
            Ray ray = new Ray(10, 0, 0, 1);

            // 'output'
            bool intersect = false;
            float t = 0;

            for (int j = 0; j < iterations; j += 2)
            {
                // Step 0: Work everything out on paper!

                // Step 2: compute the substitutions.
                float p = ray.ox - cir.px;
                float q = ray.oy - cir.py;

                float r = 2 * p * ray.dx;
                float s = 2 * q * ray.dy;

                // Step 3: compute the substitutions, check if there is a collision.
                float a = ray.dx * ray.dx + ray.dy * ray.dy;
                float b = r + s;
                float c = p * p + q * q - cir.radius * cir.radius;

                float DSqrt = b * b - 4 * a * c;

                // no collision possible!
                if (DSqrt < 0)
                { intersect = false; }

                // Step 4: compute the substitutions.
                float D = (float)Math.Sqrt(DSqrt);

                //float t0 = (-b + D) / (2 * a);
                float t1 = (-b - D) / (2 * a);

                //float ti = Math.Min(t0, t1);
                if (t1 > 0 && t1 < t)
                {
                    t = t1;
                    intersect = true;
                }

                intersect = false;
            }
        }

        [Benchmark(Description = "Ray to circle interleaved")]
        [Arguments(numberOfElements)]
        public void LatencyCircleA(int iterations)
        {
            // input
            Circle cir = new Circle(10, 10, 5);
            Ray ray = new Ray(10, 0, 0, 1);

            // 'output'
            bool intersect = false;
            float t = 0;

            for (int j = 0; j < iterations; j += 2)
            {
                // Step 0: Work everything out on paper!

                // Step 2: compute the substitutions.
                float pA = ray.ox - cir.px;
                float qA = ray.oy - cir.py;

                float rA = 2 * pA * ray.dx;
                float sA = 2 * qA * ray.dy;

                // Step 3: compute the substitutions, check if there is a collision.
                float aA = ray.dx * ray.dx + ray.dy * ray.dy;
                float bA = rA + sA;
                float cA = pA * pA + qA * qA - cir.radius * cir.radius;

                float DSqrtA = bA * bA - 4 * aA * cA;

                // no collision possible!
                if (DSqrtA < 0)
                { intersect = intersect || false; }

                // Step 4: compute the substitutions.
                float DA = MathF.Sqrt(DSqrtA);

                // Step 2: compute the substitutions.
                float pB = ray.ox - cir.px;
                float qB = ray.oy - cir.py;

                float rB = 2 * pB * ray.dx;
                float sB = 2 * qB * ray.dy;

                // Step 3: compute the substitutions, check if there is a collision.
                float aB = ray.dx * ray.dx + ray.dy * ray.dy;
                float bB = rB + sB;
                float cB = pB * pB + qB * qB - cir.radius * cir.radius;

                float DSqrtB = bB * bB - 4 * aB * cB;

                // no collision possible!
                if (DSqrtB < 0)
                { intersect = intersect || false; }

                // Step 4: compute the substitutions.
                float DB = MathF.Sqrt(DSqrtB);

                //float t0 = (-b + D) / (2 * a);
                float t1A = (-bA - DA) / (2 * aA);

                //float ti = Math.Min(t0, t1);
                if (t1A > 0 && t1A < t)
                {
                    t = t1A;
                    intersect = true;
                }

                //float t0 = (-b + D) / (2 * a);
                float t1B = (-bB - DB) / (2 * aB);

                //float ti = Math.Min(t0, t1);
                if (t1B > 0 && t1B < t)
                {
                    t = t1B;
                    intersect = true;
                }
            }
        }

        [Benchmark(Description = "Ray to circle interleaved v2")]
        [Arguments(numberOfElements)]
        public void LatencyCircleB(int iterations)
        {
            // input
            Circle cir = new Circle(10, 10, 5);
            Ray ray = new Ray(10, 0, 0, 1);

            // 'output'
            bool intersect = false;
            float t = 0;

            for (int j = 0; j < iterations; j += 2)
            {
                // Step 0: Work everything out on paper!

                // Step 2: compute the substitutions.
                float pA = ray.ox - cir.px;
                float qA = ray.oy - cir.py;
                float pB = ray.ox - cir.px;
                float qB = ray.oy - cir.py;

                float rA = 2 * pA * ray.dx;
                float sA = 2 * qA * ray.dy;

                float rB = 2 * pB * ray.dx;
                float sB = 2 * qB * ray.dy;

                // Step 3: compute the substitutions, check if there is a collision.
                float aA = ray.dx * ray.dx + ray.dy * ray.dy;
                float bA = rA + sA;
                float cA = pA * pA + qA * qA - cir.radius * cir.radius;

                float aB = ray.dx * ray.dx + ray.dy * ray.dy;
                float bB = rB + sB;
                float cB = pB * pB + qB * qB - cir.radius * cir.radius;

                float DSqrtA = bA * bA - 4 * aA * cA;
                float DSqrtB = bB * bB - 4 * aB * cB;

                // no collision possible!
                if (DSqrtA < 0)
                { intersect = intersect || false; }

                if (DSqrtB < 0)
                { intersect = intersect || false; }

                // Step 4: compute the substitutions.
                float DA = MathF.Sqrt(DSqrtA);
                float DB = MathF.Sqrt(DSqrtB);

                float t1A = (-bA - DA) / (2 * aA);
                float t1B = (-bB - DB) / (2 * aB);

                if (t1A > 0 && t1A < t)
                {
                    t = t1A;
                    intersect = true;
                }

                if (t1B > 0 && t1B < t)
                {
                    t = t1B;
                    intersect = true;
                }
            }
        }
    }
}