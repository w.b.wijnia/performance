﻿using System;
using System.Collections.Generic;
using System.Text;

using BenchmarkDotNet.Attributes;

namespace performance
{
    //[ShortRunJob]
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    public class DependenciesBenchmarks
    {
        // take note that the number of elements must be reasonable huge
        // to see the impacts of these measurements properly. The array is
        // ( 1 << 24) * sizeof(int) big, which is: 67,108864 mb
        const int numberOfElements = 1 << 24;
        int[] ai = new int[numberOfElements];
        float[] af = new float[numberOfElements];

        public DependenciesBenchmarks()
        {
            for (int j = 0; j < numberOfElements; j++)
                ai[j] = j;

            for (int j = 0; j < numberOfElements; j++)
                af[j] = j;
        }

        [Benchmark(Description = "example dependency load")]
        [Arguments(numberOfElements)]
        public void DependenciesSmall(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                ai[0]++;
                ai[0]++;
            }
        }

        [Benchmark(Description = "example no dependency load")]
        [Arguments(numberOfElements)]
        public void NoDependenciesSmall(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                ai[0]++;
                ai[1]++;
            }
        }

        [Benchmark(Description = "medium dependency load")]
        [Arguments(numberOfElements)]
        public void DependenciesMedium(int iterations)
        {
            int[] b = new int[16] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

            for (int j = 0; j < iterations; j++)
            {
                ai[0] += b[0];
                ai[0] += b[1];
                ai[0] += b[2];
                ai[0] += b[3];
                ai[0] += b[4];
                ai[0] += b[5];
                ai[0] += b[6];
                ai[0] += b[7];
                ai[0] += b[8];
                ai[0] += b[9];
                ai[0] += b[10];
                ai[0] += b[11];
                ai[0] += b[12];
                ai[0] += b[13];
                ai[0] += b[14];
                ai[0] += b[15];
            }
        }

        [Benchmark(Description = "medium no dependency load")]
        [Arguments(numberOfElements)]
        public void NoDependenciesMedium(int iterations)
        {
            int[] b = new int[16] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

            for (int j = 0; j < iterations; j++)
            {
                int b01 = b[0] + b[1];
                int b23 = b[2] + b[3];
                int b45 = b[4] + b[5];
                int b67 = b[6] + b[7];
                int b89 = b[8] + b[9];
                int b1011 = b[10] + b[11];
                int b1213 = b[12] + b[13];
                int b1415 = b[14] + b[15];

                int b01_23 = b01 + b23;
                int b45_67 = b45 + b67;
                int b89_1011 = b89 + b1011;
                int b1213_1415 = b1213 + b1415;

                int b01_23_b45_67 = b01_23 + b45_67;
                int b89_1011_b1213_1415 = b89_1011 + b1213_1415;
                
                ai[0] += b01_23_b45_67 + b89_1011_b1213_1415;
            }
        }

        [Benchmark(Description = "medium dependency load stack")]
        [Arguments(numberOfElements)]
        public void DependenciesMediumStack(int iterations)
        {
            Span<int> b = stackalloc int[16] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

            for (int j = 0; j < iterations; j++)
            {
                ai[0] += b[0];
                ai[0] += b[1];
                ai[0] += b[2];
                ai[0] += b[3];
                ai[0] += b[4];
                ai[0] += b[5];
                ai[0] += b[6];
                ai[0] += b[7];
                ai[0] += b[8];
                ai[0] += b[9];
                ai[0] += b[10];
                ai[0] += b[11];
                ai[0] += b[12];
                ai[0] += b[13];
                ai[0] += b[14];
                ai[0] += b[15];
            }
        }

        [Benchmark(Description = "medium no dependency load stack")]
        [Arguments(numberOfElements)]
        public void NoDependenciesMediumStack(int iterations)
        {
            Span<int> b = stackalloc int[16] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

            for (int j = 0; j < iterations; j++)
            {
                int b01 = b[0] + b[1];
                int b23 = b[2] + b[3];
                int b45 = b[4] + b[5];
                int b67 = b[6] + b[7];
                int b89 = b[8] + b[9];
                int b1011 = b[10] + b[11];
                int b1213 = b[12] + b[13];
                int b1415 = b[14] + b[15];

                int b01_23 = b01 + b23;
                int b45_67 = b45 + b67;
                int b89_1011 = b89 + b1011;
                int b1213_1415 = b1213 + b1415;

                int b01_23_b45_67 = b01_23 + b45_67;
                int b89_1011_b1213_1415 = b89_1011 + b1213_1415;

                ai[0] += b01_23_b45_67 + b89_1011_b1213_1415;
            }
        }
    }
}
