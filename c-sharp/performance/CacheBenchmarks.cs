﻿using System;
using System.Collections.Generic;
using System.Text;

using BenchmarkDotNet.Attributes;

namespace performance
{
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    [ShortRunJob]
    public class CacheBenchmarks
    {
        // take note that the number of elements must be reasonable huge
        // to see the impacts of these measurements properly. The array is
        // ( 1 << 24) * sizeof(int) big, which is: 67,108864 mb
        const int numberOfElements = 1 << 24;
        int[] ai = new int[numberOfElements];
        float[] af = new float[numberOfElements];

        public CacheBenchmarks()
        {
            for (int j = 0; j < numberOfElements; j++)
                ai[j] = j;

            for (int j = 0; j < numberOfElements; j++)
                af[j] = j;
        }

        [Benchmark(Description = "16 operations per 16 elements")]
        public void MemoryAccessLimitedA()
        {
            for(int j = 0; j < numberOfElements; j++)
            {
                ai[j] += 1;
            }
        }

        [Benchmark(Description = "1 operation per 16 elements")]
        public void MemoryAccessLimitedB()
        {
            for (int j = 0; j < numberOfElements; j += 16)
            {
                ai[j] += 1;
            }
        }

        [Benchmark(Description = "generalized principle")]
        [Arguments(1)]
        [Arguments(2)]
        [Arguments(4)]
        [Arguments(8)]
        [Arguments(16)]
        [Arguments(32)]
        [Arguments(64)]
        [Arguments(128)]
        [Arguments(256)]
        [Arguments(512)]
        public void MemoryAccessLimitedC(int step)
        {
            for (int j = 0; j < numberOfElements; j += step)
            {
                ai[j] += 1;
            }
        }

        // 1 kilobyte = 1024 bytes
        // 1 integer = 4 bytes

        // for the L1
        // 32kb / 4 = 8kb ~ 8192 bytes
        // 64kb / 4 = 16kb ~ 16384 bytes

        // for the L2
        // 4mb / 4 = 1.0mb ~ 1 << 20
        // 8mb / 4 = 2.0mb ~ 1 << 21
        [Benchmark(Description = "cache size analysis")]
        [Arguments(1 << 12, numberOfElements)]      // 16 kb
        [Arguments(1 << 13, numberOfElements)]      // 32 kb
        [Arguments(1 << 14, numberOfElements)]      // 64 kb
        [Arguments(1 << 15, numberOfElements)]      // 128 kb
        [Arguments(1 << 19, numberOfElements)]      // 0.5 mb
        [Arguments(1 << 20, numberOfElements)]      // 1.0 mb
        [Arguments(1 << 21, numberOfElements)]      // 2.0 mb
        [Arguments(1 << 22, numberOfElements)]      // 4.0 mb
        public void CacheSize(int modulo, int iterations)
        {
            for(int j = 0; j < iterations; j++)
            {
                ai[(j * 16) & (modulo - 1)]++;
            }
        }
    }
}
