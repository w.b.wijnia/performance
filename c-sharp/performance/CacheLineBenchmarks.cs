﻿using System;
using System.Collections.Generic;
using System.Text;

using BenchmarkDotNet.Attributes;

namespace performance
{
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    public class CacheLineBenchmarks
    {

        private struct LongStruct
        {
            public int i1, i2, i3, i4;
            int i5, i6, i7, i8;
        }

        private struct OffStruct
        {
            public int i1, i2, i3, i4;
            public int i5, i6, i7;
        }

        private struct ShortStruct
        {
            public int i1, i2;
        }

        // take note that the number of elements must be reasonable huge
        // to see the impacts of these measurements properly. The array is
        // ( 1 << 24) * sizeof(int) big, which is: 67,108864 mb
        const int numberOfElements = 1 << 24;
        LongStruct[] longElements = new LongStruct[numberOfElements];
        ShortStruct[] shortElements = new ShortStruct[numberOfElements];
        OffStruct[] offElements = new OffStruct[numberOfElements];

        public CacheLineBenchmarks()
        {
            for (int j = 0; j < numberOfElements; j++)
                longElements[j] = new LongStruct();

            for (int j = 0; j < numberOfElements; j++)
                shortElements[j] = new ShortStruct();

            for (int j = 0; j < numberOfElements; j++)
                offElements[j] = new OffStruct();
        }

        [Benchmark(Description = "One element (long)")]
        [Arguments(numberOfElements)]
        public void CacheLineLong(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                longElements[j].i1++;
            }
        }

        [Benchmark(Description = "One element (short)")]
        [Arguments(numberOfElements)]
        public void CacheLineShort(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                shortElements[j].i1++;
            }
        }

        [Benchmark(Description = "struct not size of power of two")]
        [Arguments(numberOfElements, 0)]
        [Arguments(numberOfElements, 1)]
        [Arguments(numberOfElements, 2)]
        [Arguments(numberOfElements, 3)]
        [Arguments(numberOfElements, 4)]
        [Arguments(numberOfElements, 5)]
        [Arguments(numberOfElements, 6)]
        [Arguments(numberOfElements, 7)]
        [Arguments(numberOfElements, 8)]
        [Arguments(numberOfElements, 9)]
        [Arguments(numberOfElements, 10)]
        [Arguments(numberOfElements, 11)]
        [Arguments(numberOfElements, 12)]
        [Arguments(numberOfElements, 13)]
        [Arguments(numberOfElements, 14)]
        [Arguments(numberOfElements, 15)]
        public void CacheLineOff(int iterations, int index)
        {
            for (int j = 0; j < iterations; j++)
            {
                offElements[index].i1++;
                offElements[index].i2++;
                offElements[index].i3++;
                offElements[index].i4++;
                offElements[index].i5++;
                offElements[index].i6++;
                offElements[index].i7++;
            }
        }
    }
}
