﻿using System;
using System.Collections.Generic;
using System.Text;

using BenchmarkDotNet.Attributes;

namespace performance
{
    [ShortRunJob]
    [DisassemblyDiagnoser(printAsm: true, printSource: true)]
    public class InterleavingBenchmarks
    {

        // take note that the number of elements must be reasonable huge
        // to see the impacts of these measurements properly. The array is
        // ( 1 << 24) * sizeof(int) big, which is: 67,108864 mb
        const int numberOfElements = 1 << 24;
        int[] ai = new int[numberOfElements];
        float[] af = new float[numberOfElements];

        public InterleavingBenchmarks()
        {
            for (int j = 0; j < numberOfElements; j++)
                ai[j] = j;

            for (int j = 0; j < numberOfElements; j++)
                af[j] = j;
        }

        [Benchmark(Description = "interleaving: only integer")]
        [Arguments(numberOfElements)]
        public void InterleavingBaseInteger(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                ai[0]++;
            }
        }

        [Benchmark(Description = "interleaving: only float")]
        [Arguments(numberOfElements)]
        public void InterleavingBaseFloat(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0]++;
            }
        }

        [Benchmark(Description = "interleaving: float and integer")]
        [Arguments(numberOfElements)]
        public void InterleavingFloatInteger(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                ai[0]++;
                af[0]++;
            }
        }

        [Benchmark(Description = "interleaving: integer and integer")]
        [Arguments(numberOfElements)]
        public void InterleavingIntegerInteger(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                ai[0]++;
                ai[1]++;
            }
        }

        [Benchmark(Description = "interleaving: float and float")]
        [Arguments(numberOfElements)]
        public void InterleavingFloatFloat(int iterations)
        {
            for (int j = 0; j < iterations; j++)
            {
                af[0]++;
                af[1]++;
            }
        }
    }
}
