﻿using System;

using BenchmarkDotNet.Running;
using BenchmarkDotNet.Attributes;

namespace performance
{
    class Program
    {

        static void Main(string[] args)
        {

            BenchmarkSwitcher.FromAssembly(typeof(Program).Assembly).Run(args);
        }
    }
}
